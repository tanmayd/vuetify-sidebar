
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router';
import Vuetify from 'vuetify';
import router from './routes';
Vue.use(VueRouter);
Vue.use(Vuetify,{
    theme: {
      primary: '#243D8F',
      secondary: '#446699',
      accent: '#82B1FF',
      error: '#FF5252',
      info: '#2196F3',
      success: '#4CAF50',
      warning: '#FFC107'
    }
});
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
// Vue.component('home', require('./views/Home.vue'));
router.beforeEach((to, from, next) => {
  var requiresAuth = to.matched.some( record => record.meta.requiresAuth );
  var currentUser = false;
  if(requiresAuth && !currentUser){
    try{
      next('/login');
    } catch (e){
      console.log(e.message);
    }
  } else if (to.path == '/login' && currentUser){
    next('/');
  } else {
    next();
  }
});
const app = new Vue({
    router,
    el: '#app'
});
