import VueRouter from 'vue-router';

let routes = [
    { path: '/', component: require('./views/Home'), meta: { requiresAuth: true }, children: [{ path: '/demo', component: require('./views/Demo') }]},
    { path: '/login', component: require('./views/Login') },
];

export default new VueRouter({
    routes
});